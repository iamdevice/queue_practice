<?php
/**
 * Created by Aleksandr Kozhevnikov <iamdevice@gmail.com>
 * Date: 2018-10-21 13:45
 */

namespace SimpleEmailQueue\Tests;

use OldSound\RabbitMqBundle\Event\AfterProcessingMessageEvent;
use OldSound\RabbitMqBundle\Event\BeforeProcessingMessageEvent;
use OldSound\RabbitMqBundle\RabbitMq\Consumer;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use SimpleEmailQueue\Consumer\MessageSenderConsumer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ConsumerTest extends KernelTestCase
{
    /**
     * @var SerializerInterface $serializer
     */
    private $serializer;

    public function setUp()
    {
        self::createKernel();
        self::bootKernel();
        $this->serializer = self::$container->get(SerializerInterface::class);
    }

    public function testSuccessProcessMessage(): void
    {
        $amqpConnection = $this->prepareAMQPConnection();
        $amqpChannel = $this->prepareAMQPChannel();
        $amqpChannel
            ->expects($this->once())
            ->method('basic_ack');

        /** @var AMQPChannel $amqpChannel */
        $consumer = $this->getConsumer($amqpConnection, $amqpChannel);
        $consumer->disableAutoSetupFabric();
        $consumer->setChannel($amqpChannel);
        $consumer->setCallback([
            $this->prepareMessageConsumer(),
            'execute'
        ]);

        $amqpMessage = $this->prepareAmqpMessage($amqpChannel);

        $eventDispatcher = $this
            ->getMockBuilder(EventDispatcherInterface::class)
            ->getMock();
        $eventDispatcher->expects($this->atLeastOnce())
            ->method('dispatch')
            ->withConsecutive(
                [ BeforeProcessingMessageEvent::NAME, new BeforeProcessingMessageEvent($consumer, $amqpMessage) ],
                [ AfterProcessingMessageEvent::NAME, new AfterProcessingMessageEvent($consumer, $amqpMessage) ]
            )
            ->willReturn(true);
        /** @var EventDispatcherInterface $eventDispatcher */
        $consumer->setEventDispatcher($eventDispatcher);

        $consumer->processMessage($amqpMessage);
    }

    public function testFailProcessMessage(): void
    {
        $amqpConnection = $this->prepareAMQPConnection();
        $amqpChannel = $this->prepareAMQPChannel();
        $amqpChannel
            ->expects($this->never())
            ->method('basic_ack');
        $amqpChannel
            ->expects($this->once())
            ->method('basic_reject');

        /** @var AMQPChannel $amqpChannel */
        $consumer = $this->getConsumer($amqpConnection, $amqpChannel);
        $consumer->disableAutoSetupFabric();
        $consumer->setChannel($amqpChannel);

        /** @var MessageSenderConsumer $messageConsumer */
        $messageConsumer = self::$container->get(MessageSenderConsumer::class);
        $errorMsg = "Value 'sms' is not part of the enum SimpleEmailQueue\Enum\MessageType";
        $logger = $this
            ->getMockBuilder(LoggerInterface::class)
            ->getMock();
        $logger
            ->expects($this->once())
            ->method('error')
            ->with(
                $errorMsg,
                [
                    'exception' => new \UnexpectedValueException($errorMsg)
                ]
            );
        /** @var LoggerInterface $logger */
        $messageConsumer->setLogger($logger);
        $consumer->setCallback([
            $messageConsumer,
            'execute'
        ]);

        $amqpMessage = new AMQPMessage('{"type": "sms"}');
        $amqpMessage->delivery_info['channel'] = $amqpChannel;
        $amqpMessage->delivery_info['delivery_tag'] = 0;

        $eventDispatcher = $this
            ->getMockBuilder(EventDispatcherInterface::class)
            ->getMock();
        $eventDispatcher
            ->expects($this->atLeastOnce())
            ->method('dispatch')
            ->withConsecutive(
                [ BeforeProcessingMessageEvent::NAME, new BeforeProcessingMessageEvent($consumer, $amqpMessage) ],
                [ AfterProcessingMessageEvent::NAME, new AfterProcessingMessageEvent($consumer, $amqpMessage) ]
            )
            ->willReturn(true);
        /** @var EventDispatcherInterface $eventDispatcher */
        $consumer->setEventDispatcher($eventDispatcher);

        $consumer->processMessage($amqpMessage);
    }

    protected function getConsumer($amqpConnection, $amqpChannel): Consumer
    {
        return new Consumer($amqpConnection, $amqpChannel);
    }

    protected function prepareAMQPConnection(): MockObject
    {
        return $this
            ->getMockBuilder(AMQPConnection::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function prepareAMQPChannel(): MockObject
    {
        return $this
            ->getMockBuilder(AMQPChannel::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function prepareAmqpMessage(AMQPChannel $amqpChannel): AMQPMessage
    {
        $amqpMessage = new AMQPMessage(
            file_get_contents(__DIR__ . '/_data/message.json'),
            [
                'content_type' => 'json'
            ]
        );
        $amqpMessage->delivery_info['channel'] = $amqpChannel;
        $amqpMessage->delivery_info['delivery_tag'] = 0;

        return $amqpMessage;
    }

    protected function prepareMessageConsumer(): MessageSenderConsumer
    {
        $transport = $this
            ->getMockBuilder(\Swift_Transport::class)
            ->disableOriginalConstructor()
            ->getMock();

        $transport
            ->expects($this->once())
            ->method('send')
            ->willReturn(1);

        /** @var \Swift_Transport $transport */
        return new MessageSenderConsumer(
            $this->serializer,
            new \Swift_Mailer($transport)
        );
    }
}
