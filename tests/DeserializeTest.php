<?php
/**
 * Created by Aleksandr Kozhevnikov <iamdevice@gmail.com>
 * Date: 2018-10-21 17:59
 */

namespace SimpleEmailQueue\Tests;

use SimpleEmailQueue\Entity\Message;
use SimpleEmailQueue\Enum\MessageType;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\SerializerInterface;

class DeserializeTest extends KernelTestCase
{
    /**
     * @var SerializerInterface $serializer
     */
    private $serializer;

    public function setUp()
    {
        $this->serializer = self::$container->get(SerializerInterface::class);
    }

    public static function setUpBeforeClass()
    {
        self::createKernel();
        self::bootKernel();
    }

    public function testDeserializeMessage(): void
    {
        $msg = file_get_contents(__DIR__ . '/_data/message.json');
        /** @var Message $deserialized */
        $deserialized = $this->serializer->deserialize($msg, Message::class, 'json');

        $this->assertEquals(MessageType::EMAIL()->getValue(), $deserialized->getType());
        $this->assertEquals('from@test.com', $deserialized->getFrom());
        $this->assertEquals('to@test.org', $deserialized->getTo());
        $this->assertEquals('Some subject', $deserialized->getSubject());
        $this->assertEquals('Hello world!', $deserialized->getMessage());
    }
}
