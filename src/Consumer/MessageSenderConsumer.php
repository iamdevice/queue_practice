<?php
/**
 * Created by Aleksandr Kozhevnikov <iamdevice@gmail.com>
 * Date: 2018-10-21 13:51
 */

namespace SimpleEmailQueue\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use SimpleEmailQueue\Entity\Message;
use SimpleEmailQueue\Enum\MessageType;
use SimpleEmailQueue\Exception\UnsupportedMessageTypeException;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;

class MessageSenderConsumer implements ConsumerInterface
{
    use LoggerAwareTrait;

    /**
     * @var SerializerInterface $serializer
     */
    private $serializer;

    /**
     * @var \Swift_Mailer $mailer
     */
    private $mailer;

    public function __construct(SerializerInterface $serializer, \Swift_Mailer $mailer)
    {
        $this->serializer = $serializer;
        $this->mailer = $mailer;
        $this->logger = new NullLogger();
    }

    public function execute(AMQPMessage $msg)
    {
        try {
            /** @var Message $queueMessage */
            $queueMessage = $this->serializer->deserialize($msg->getBody(), Message::class, 'json');
            $messageType = new MessageType($queueMessage->getType());
            /** @var \Swift_Message $emailMessage */
            $emailMessage = $messageType->getConverter()->convert($queueMessage);
            $this->mailer->send($emailMessage);

            $flag = ConsumerInterface::MSG_ACK;
        } catch (UnsupportedMessageTypeException | NotEncodableValueException $exception) {
            $this->logger->error($exception->getMessage(), ['exception' => $exception]);

            $flag = ConsumerInterface::MSG_REJECT;
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage(), ['exception' => $exception]);

            $flag = ConsumerInterface::MSG_REJECT_REQUEUE;
        }
        gc_collect_cycles();

        return $flag;
    }
}
