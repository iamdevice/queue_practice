<?php
/**
 * Created by Aleksandr Kozhevnikov <iamdevice@gmail.com>
 * Date: 2018-10-21 18:42
 */

namespace SimpleEmailQueue\Service;

use SimpleEmailQueue\Entity\Message;

interface MessageConverterInterface
{
    /**
     * @param Message $message
     *
     * @return object
     */
    public function convert(Message $message);
}
