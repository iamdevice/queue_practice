<?php
/**
 * Created by Aleksandr Kozhevnikov <iamdevice@gmail.com>
 * Date: 2018-10-21 18:42
 */

namespace SimpleEmailQueue\Service;

use SimpleEmailQueue\Entity\Message;
use SimpleEmailQueue\Enum\MessageType;
use SimpleEmailQueue\Exception\UnsupportedMessageTypeException;

class MessageToEmailConverter implements MessageConverterInterface
{
    /**
     * @param Message $message
     *
     * @return \Swift_Message
     */
    public function convert(Message $message): \Swift_Message
    {
        if ($message->getType() !== MessageType::EMAIL()->getValue()) {
            throw new UnsupportedMessageTypeException("Unsupported type {$message->getType()} in email converter");
        }

        $emailMessage = new \Swift_Message($message->getSubject(), $message->getMessage());
        $emailMessage->setFrom($message->getFrom());
        $emailMessage->setTo($message->getTo());

        return $emailMessage;
    }
}
