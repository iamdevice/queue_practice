<?php
/**
 * Created by Aleksandr Kozhevnikov <iamdevice@gmail.com>
 * Date: 2018-10-21 18:35
 */

namespace SimpleEmailQueue\Enum;

use MyCLabs\Enum\Enum;
use SimpleEmailQueue\Service\MessageConverterInterface;
use SimpleEmailQueue\Service\MessageToEmailConverter;

/**
 * Class MessageEnum
 * @package SimpleEmailQueue\Enum
 *
 * @method static \SimpleEmailQueue\Enum\MessageType EMAIL
 */
class MessageType extends Enum
{
    public const EMAIL = 'email';

    private $converterMap = [
        self::EMAIL => MessageToEmailConverter::class,
    ];

    public function getConverter(): MessageConverterInterface
    {
        return new $this->converterMap[$this->value]();
    }
}
