<?php
/**
 * Created by Aleksandr Kozhevnikov <iamdevice@gmail.com>
 * Date: 2018-10-21 21:49
 */

namespace SimpleEmailQueue\Exception;

class UnsupportedMessageTypeException extends \InvalidArgumentException
{

}
